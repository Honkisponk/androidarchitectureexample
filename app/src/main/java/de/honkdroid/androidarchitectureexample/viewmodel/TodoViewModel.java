package de.honkdroid.androidarchitectureexample.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import de.honkdroid.androidarchitectureexample.model.Todo;
import de.honkdroid.androidarchitectureexample.model.TodoRepository;

public class TodoViewModel extends AndroidViewModel {

    private final TodoRepository repository;
    private final LiveData<List<Todo>> todoList;

    public TodoViewModel(@NonNull Application application) {
        super(application);

        repository = new TodoRepository(application);
        todoList = repository.getAll();
    }

    public void addTodo(Todo todo) {
        repository.insert(todo);
    }

    public void editTodo(Todo todo) {
        repository.update(todo);
    }

    public void deleteTodo(Todo category) {
        repository.delete(category);
    }

    public LiveData<List<Todo>> getTodoList() {
        return todoList;
    }
}

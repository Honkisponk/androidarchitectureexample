package de.honkdroid.androidarchitectureexample.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.honkdroid.androidarchitectureexample.R;

public class TodoActivity extends AppCompatActivity {

    public static final String EXTRA_ID = "EXTRA_ID";
    public static final String EXTRA_TITLE = "EXTRA_TITLE";
    public static final String EXTRA_PRIORITY = "EXTRA_PRIORITY";

    @BindView(R.id.edittext_todo_title)
    EditText titleEditText;

    @BindView(R.id.edittext_todo_priority)
    EditText priorityEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);
        ButterKnife.bind(this);

        if (getIntent().hasExtra(EXTRA_ID)) {
            String title = getIntent().getStringExtra(TodoActivity.EXTRA_TITLE);
            int priority = getIntent().getIntExtra(TodoActivity.EXTRA_PRIORITY, 0);
            titleEditText.setText(title);
            priorityEditText.setText(String.valueOf(priority));
        }
    }

    @OnClick(R.id.button_todo_add)
    public void onClickAddTodoButton() {
        String titleText = titleEditText.getText().toString();
        String priorityText = priorityEditText.getText().toString();

        if (titleText.trim().length() == 0 || priorityText.trim().length() == 0) {
            Toast.makeText(this, getString(R.string.todo_errormessage), Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_ID, getIntent().getIntExtra(EXTRA_ID, -1));
        data.putExtra(EXTRA_TITLE, titleText);
        data.putExtra(EXTRA_PRIORITY, Integer.valueOf(priorityText));
        setResult(RESULT_OK, data);
        finish();
    }
}

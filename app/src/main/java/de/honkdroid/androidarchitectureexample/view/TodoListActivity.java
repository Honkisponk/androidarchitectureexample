package de.honkdroid.androidarchitectureexample.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.honkdroid.androidarchitectureexample.R;
import de.honkdroid.androidarchitectureexample.model.Todo;
import de.honkdroid.androidarchitectureexample.viewmodel.TodoViewModel;

public class TodoListActivity extends AppCompatActivity implements ItemSwipeCallback.OnItemSwipeListener {

    private static final int ADD_TODO_REQUEST = 1;
    private static final int EDIT_TODO_REQUEST = 2;

    @BindView(R.id.recyclerview_todolist_todos)
    RecyclerView recyclerView;

    private TodoAdapter adapter;
    private TodoViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todolist);
        ButterKnife.bind(this);

        adapter = new TodoAdapter();
        adapter.setOnItemClickListener(this::onClickTodoItem);

        viewModel = ViewModelProviders.of(this).get(TodoViewModel.class);
        viewModel.getTodoList().observe(this, adapter::submitList);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        new ItemTouchHelper(new ItemSwipeCallback(this)).attachToRecyclerView(recyclerView);
    }

    @OnClick(R.id.button_todolist_addbutton)
    public void onClickAddTodoButton() {
        Intent intent = new Intent(this, TodoActivity.class);
        startActivityForResult(intent, ADD_TODO_REQUEST);
    }

    private void onClickTodoItem(Todo todo) {
        Intent intent = new Intent(this, TodoActivity.class);
        intent.putExtra(TodoActivity.EXTRA_ID, todo.getId());
        intent.putExtra(TodoActivity.EXTRA_TITLE, todo.getTitle());
        intent.putExtra(TodoActivity.EXTRA_PRIORITY, todo.getPriority());
        startActivityForResult(intent, EDIT_TODO_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            int id = data.getIntExtra(TodoActivity.EXTRA_ID, -1);
            String title = data.getStringExtra(TodoActivity.EXTRA_TITLE);
            int priority = data.getIntExtra(TodoActivity.EXTRA_PRIORITY, 0);

            if (requestCode == ADD_TODO_REQUEST) {
                viewModel.addTodo(new Todo(title, priority));
            } else if (requestCode == EDIT_TODO_REQUEST && id != -1) {
                viewModel.editTodo(new Todo(id, title, priority));
            }
        }
    }

    @Override
    public void onItemSwipe(int position) {
        viewModel.deleteTodo(adapter.getItemAt(position));
    }
}

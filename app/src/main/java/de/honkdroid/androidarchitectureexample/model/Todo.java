package de.honkdroid.androidarchitectureexample.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity(tableName = "todo")
@AllArgsConstructor
@Getter
@Setter
public class Todo {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "priority")
    private int priority;

    public Todo(String title, int priority) {
        this.title = title;
        this.priority = priority;
    }
}

package de.honkdroid.androidarchitectureexample.model;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Todo.class}, version = 1)
abstract class AppDatabase extends RoomDatabase {

    private static final String DB_NAME = "AndroidArchitectureExample.db";
    private static volatile AppDatabase INSTANCE;

    static synchronized AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDatabase.class,
                    DB_NAME).build();
        }
        return INSTANCE;
    }

    abstract TodoDao todoDao();
}

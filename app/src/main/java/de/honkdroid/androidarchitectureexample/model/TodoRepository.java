package de.honkdroid.androidarchitectureexample.model;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import lombok.AllArgsConstructor;

public class TodoRepository {

    private final TodoDao dao;
    private final LiveData<List<Todo>> all;

    public TodoRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        dao = database.todoDao();
        all = dao.getAll();
    }

    public void insert(Todo todo) {
        new InsertAsyncTask(dao).execute(todo);
    }

    public void update(Todo todo) {
        new UpdateAsyncTask(dao).execute(todo);
    }

    public void delete(Todo todo) {
        new DeleteAsyncTask(dao).execute(todo);
    }

    public LiveData<List<Todo>> getAll() {
        return all;
    }

    @AllArgsConstructor
    private static class InsertAsyncTask extends AsyncTask<Todo, Void, Void> {
        private TodoDao dao;

        @Override
        protected Void doInBackground(Todo... all) {
            dao.insert(all[0]);
            return null;
        }
    }

    @AllArgsConstructor
    private static class UpdateAsyncTask extends AsyncTask<Todo, Void, Void> {
        private TodoDao dao;

        @Override
        protected Void doInBackground(Todo... all) {
            dao.update(all[0]);
            return null;
        }
    }

    @AllArgsConstructor
    private static class DeleteAsyncTask extends AsyncTask<Todo, Void, Void> {
        private TodoDao dao;

        @Override
        protected Void doInBackground(Todo... all) {
            dao.delete(all[0]);
            return null;
        }
    }
}
